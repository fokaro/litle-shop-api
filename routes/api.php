<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarchandiseController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
    
});
Route::get('/get-marchandise', [MarchandiseController::class, 'getAll']);
Route::post('/get-one-marchandise', [MarchandiseController::class, 'getOne']);
Route::post('/create', [MarchandiseController::class, 'createMarchandise']);
Route::post('/delete-marchandise', [MarchandiseController::class, 'delete']);
Route::post('/update-marchandise', [MarchandiseController::class, 'updateMarchandise']);
Route::post('/table', [MarchandiseController::class, 'research']);




